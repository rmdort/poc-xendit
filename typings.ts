export type University = {
  name: string;
  country: string;
  web_pages: string[];
  alpha_two_code: string;
  "state-province": string | null;
  domains: string[];
};

export type Country = {
  value: string;
  label: string;
};

export type SortOption = {
  value: keyof University;
  label: string;
};
