import { Heading, Text } from "@chakra-ui/react";
import React from "react";
import { Layout } from "../components/layout";

const Page = () => {
  return (
    <Layout title="404 Page not found" hideNavigation>
      <Text>The page you requested could not be found</Text>
    </Layout>
  );
};

export default Page;
