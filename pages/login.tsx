import { Box } from "@chakra-ui/layout";
import { Form, Formik, FormikConfig } from "formik";
import React, { FC, useCallback } from "react";
import { Layout } from "../components/layout";
import { object, string } from "yup";
import { InputControl, SubmitButton } from "formik-chakra-ui";
import { useToast } from "@chakra-ui/toast";
import Cookies from "js-cookie";
import { GetServerSideProps } from "next";
import { verifyAccessToken } from "../lib/jwt";
import { User } from ".prisma/client";
import { useRouter } from "next/router";

type LoginFormModel = {
  email: string;
  password: string;
};

const validationSchema = object().shape({
  email: string()
    .email("Please enter a valid email")
    .required("This field is required"),
  password: string().required('This field is required"'),
});

type Props = {
  user?: User | null;
};

const Login: FC<Props> = ({ user }) => {
  const toast = useToast();
  const router = useRouter();
  const handleSubmit = useCallback<FormikConfig<LoginFormModel>["onSubmit"]>(
    async (values, formikHelper) => {
      const response = await fetch("/api/login", {
        method: "POST",
        body: JSON.stringify({
          email: values.email,
          password: values.password,
        }),
      })
        .then(async (response) => {
          if (!response.ok) {
            const json = await response.json();
            throw json;
          }
          return response;
        })
        .then((res) => res.json())
        .then((response) => {
          Cookies.set("token", response?.accessToken ?? "");

          router.push("/");
        })
        .catch((err) => {
          console.log(err);
          toast({
            status: "error",
            title: "Error",
            description: err.error,
          });
        });
    },
    []
  );
  return (
    <Layout title="Login to Uni" user={user}>
      <Formik<LoginFormModel>
        initialValues={{
          email: "",
          password: "",
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <Form>
          <Box
            p={[4, 4, 12]}
            bg="gray.50"
            boxShadow="xs"
            maxWidth="container.sm"
            ml="auto"
            mr="auto"
          >
            <Box pb={2}>
              <InputControl
                inputProps={{ bg: "white" }}
                name="email"
                label="Email"
                isRequired
                mb={2}
              />
              <InputControl
                inputProps={{
                  bg: "white",
                  type: "password",
                  "aria-label": "password-input",
                }}
                name="password"
                label="Password"
                isRequired
                mb={2}
              />
            </Box>
            <SubmitButton data-testid="button-submit" colorScheme="linkedin">
              Login
            </SubmitButton>
          </Box>
        </Form>
      </Formik>
    </Layout>
  );
};
/* istanbul ignore next */
export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req } = context;
  const token = req.cookies.token;
  let user = null;
  try {
    if (token) {
      user = await (await verifyAccessToken(token)).payload;
    }
  } catch (err) {}
  if (user) {
    return {
      props: {
        user,
      },
      redirect: {
        destination: "/",
      },
    };
  }
  return {
    props: {
      user,
    },
  };
};

export default Login;
