// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { BASE_URL } from "../../config";
import { Country, University } from "../../typings";
import { alphaSort } from "../../utils";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Country[]>
) {
  const universities: University[] = await fetch(
    `${BASE_URL}search?name=`
  ).then((res) => res.json());
  const countries = [
    ...universities.reduce((acc, uni) => {
      acc.add(uni.country);
      return acc;
    }, new Set<string>()),
  ]
    .sort(alphaSort)
    .map((value) => ({ value, label: value }));
  res.status(200).json(countries);
}
