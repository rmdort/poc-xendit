import { NextApiRequest, NextApiResponse } from "next";
import { Authentication } from "../../lib/auth";
import { verifyAccessToken } from "../../lib/jwt";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    const { email, password } = JSON.parse(req.body);
    if (!email || !password) {
      return res.status(400).json({
        status: "error",
        error: "Request missing username or password",
      });
    }
    try {
      const response = await Authentication.login(
        email as string,
        password as string
      );
      res.status(200).json(response);
    } catch (err) {
      res.status(400).json({ success: false, error: "Imvalid credentials" });
    }
  }
}
