import { NextApiRequest, NextApiResponse } from "next";
const fs = require("fs");
const path = require("path");

const filePath = path.resolve("./public", "subscribers.json");
let subscribers: string[] = JSON.parse(fs.readFileSync(filePath, "utf8"));

function persistJSON(data: string[]) {
  fs.writeFile(filePath, JSON.stringify(data, null, 4));
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<string>
) {
  const { email } = JSON.parse(req.body);
  if (subscribers.indexOf(email) !== -1) {
    console.log("subscribers", subscribers, subscribers.indexOf(email));
    res.status(409).json("Email already exists in the database.");
  } else {
    subscribers.push(email);
    persistJSON(subscribers);
    res.status(200).json("Success");
  }
}
