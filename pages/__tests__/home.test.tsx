import { render, screen, waitFor, within } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Home from "..";
import fetchMock from "jest-fetch-mock";
import { University } from "../../typings";
import selectEvent from "react-select-event";

const mockUniversities: University[] = [
  {
    name: "B",
    country: "Singapore",
    "state-province": "SG",
    alpha_two_code: "SG",
    domains: [],
    web_pages: [],
  },
  {
    name: "A",
    country: "Japan",
    "state-province": "SG",
    alpha_two_code: "SG",
    domains: [],
    web_pages: [],
  },
];

describe("<Home />", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  it("can render successfully", () => {
    render(<Home />);
  });

  it("can trigger a uni search", async () => {
    fetchMock.mockResponse(JSON.stringify(mockUniversities));
    render(<Home />);
    userEvent.type(screen.getByTestId("query"), "singapore");
    await waitFor(() => {
      expect(screen.getByText("Singapore")).toBeTruthy();
    });
  });

  it("can sort universities", async () => {
    fetchMock.mockResponse(JSON.stringify(mockUniversities));
    render(<Home />);
    await selectEvent.select(screen.getByLabelText("Sort by"), ["Name"]);
    const firstUniversity = screen.getAllByTestId("university-item")[0];
    expect(within(firstUniversity).getByText("Japan")).toBeTruthy();
  });
});
