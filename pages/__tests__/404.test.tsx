import { render, screen } from "@testing-library/react";
import ErrorPage from "./../404";

describe("<Error />", () => {
  it("can render error page", () => {
    render(<ErrorPage />);
    expect(screen.getByText("404 Page not found")).toBeTruthy();
  });
});
