import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Register from "./../register";

const enterName = () => {
  userEvent.type(screen.getByRole("textbox", { name: "Name" }), "foobar");
};

const enterEmail = () => {
  userEvent.type(
    screen.getByRole("textbox", { name: "Email" }),
    "foo@foobar.com"
  );
};
const enterPassword = () => {
  userEvent.type(screen.getByLabelText("password-input"), "password");
};
const clickSubmit = () => {
  userEvent.click(screen.getByTestId("button-submit"));
};

describe("<Register />", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  it("can render register page", () => {
    render(<Register />);
    expect(screen.getAllByText("Register")).toBeTruthy();
  });

  it("can register user", async () => {
    fetchMock.mockResponse(
      JSON.stringify({
        name: "hello",
      })
    );
    render(<Register />);
    enterName();
    enterEmail();
    enterPassword();
    clickSubmit();
  });

  it("can display toast if login fails", async () => {
    const response = new Response(
      JSON.stringify({ error: "Duplicate email" }),
      {
        status: 400,
        statusText: "Something went wrong!",
      }
    );

    fetchMock.mockResolvedValue(response);

    render(<Register />);
    enterName();
    enterEmail();
    enterPassword();
    clickSubmit();

    await waitFor(() => {
      expect(screen.getByText("Error")).toBeTruthy();
    });
  });
});
