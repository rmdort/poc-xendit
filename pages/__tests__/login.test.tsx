import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Login from "./../login";

const enterEmail = () => {
  userEvent.type(
    screen.getByRole("textbox", { name: "Email" }),
    "foo@foobar.com"
  );
};
const enterPassword = () => {
  userEvent.type(screen.getByLabelText("password-input"), "password");
};
const clickSubmit = () => {
  userEvent.click(screen.getByTestId("button-submit"));
};

describe("<Login />", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  it("can render login page", () => {
    render(<Login />);
    expect(screen.getByText("Login to Uni")).toBeTruthy();
  });

  it("can login user", async () => {
    fetchMock.mockResponse(
      JSON.stringify({
        name: "hello",
      })
    );
    render(<Login />);
    enterEmail();
    enterPassword();
    clickSubmit();
  });

  it("can display toast if login fails", async () => {
    const response = new Response(
      JSON.stringify({ error: "Duplicate email" }),
      {
        status: 400,
        statusText: "Something went wrong!",
      }
    );

    fetchMock.mockResolvedValue(response);

    render(<Login />);
    enterEmail();
    enterPassword();
    clickSubmit();

    await waitFor(() => {
      expect(screen.getByText("Error")).toBeTruthy();
    });
  });
});
