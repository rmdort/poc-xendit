import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Subscribe from "./../subscribe";

describe("<Subscribe />", () => {
  it("can render error page", () => {
    render(<Subscribe />);
    expect(screen.getByText("Subcribe to our newsletter")).toBeTruthy();
  });

  it("will show validation error", async () => {
    render(<Subscribe />);
    const input = screen.getByRole("textbox");
    userEvent.type(input, "hello");
    userEvent.tab();
    await waitFor(() => {
      expect(screen.getByText("Please enter a valid email")).toBeTruthy();
    });
  });
});
