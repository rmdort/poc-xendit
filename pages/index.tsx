import {
  Grid,
  FormControl,
  FormLabel,
  Input,
  Divider,
  Text,
  Alert,
  Box,
  InputGroup,
  InputLeftElement,
  CircularProgress,
} from "@chakra-ui/react";
import type { GetServerSideProps, NextPage } from "next";
import React, { FC, useEffect, useMemo, useState } from "react";
import { Layout } from "../components/layout";
import Select from "react-select";
import { CountrySelect } from "../components/country-select";
import { DEFAULT_SORT_OPTIONS, PAGE_SIZE } from "../config";
import { useUniversities } from "../hooks/useUniversities";
import { useDebouncedValue } from "../hooks/useDebouncedValue";
import { SearchIcon } from "@chakra-ui/icons";
import { UniversityList } from "../components/university-list";
import { Pagination } from "../components/pagination";
import { SortOption } from "../typings";
import { verifyAccessToken } from "../lib/jwt";
import { User } from ".prisma/client";

type Props = {
  user?: User | null;
};

const Home: FC<Props> = ({ user }) => {
  const [query, setQuery] = useState("");
  const [selectedCountry, setSelectedCountry] = useState<string>();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [sortBy, setSortBy] = useState<SortOption["value"]>();
  const debouncedQuery = useDebouncedValue(query);
  const { universities, totalResults, isLoading } = useUniversities(
    debouncedQuery,
    selectedCountry,
    sortBy,
    "asc",
    (currentPage - 1) * PAGE_SIZE,
    currentPage * PAGE_SIZE
  );
  const handleQueryChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    setQuery(e.target.value);
  return (
    <Layout title="Universities" user={user}>
      <Grid
        templateColumns={{
          lg: "1fr minmax(auto, 300px)",
        }}
        gridGap={4}
        borderColor="gray.100"
        borderWidth={1}
        p={4}
        boxShadow="xs"
      >
        <FormControl>
          <FormLabel htmlFor="label-search">Search universities</FormLabel>
          <InputGroup>
            <InputLeftElement pointerEvents="none">
              {isLoading ? (
                <CircularProgress isIndeterminate size={5} />
              ) : (
                <SearchIcon color="gray.300" />
              )}
            </InputLeftElement>
            <Input
              placeholder="Enter keywords"
              value={query}
              onChange={handleQueryChange}
              autoFocus
              data-testid="query"
              id="label-search"
            />
          </InputGroup>
        </FormControl>
        <FormControl>
          <FormLabel htmlFor="select-country">Filter by country</FormLabel>
          <CountrySelect
            value={selectedCountry}
            onChange={setSelectedCountry}
            inputId="select-country"
          />
        </FormControl>
      </Grid>

      <Divider mt={4} />

      <Grid
        templateColumns={{
          lg: "1fr auto",
        }}
        gridGap={4}
        pb={4}
        pt={4}
        alignItems="center"
      >
        <Text flex={1}>Showing {totalResults} universities</Text>
        <FormControl display="flex" alignItems="center" w="auto">
          <FormLabel htmlFor="select-sort" mb={0}>
            Sort by
          </FormLabel>
          <Box minWidth={150}>
            <Select
              isSearchable={false}
              options={DEFAULT_SORT_OPTIONS}
              onChange={(item) => setSortBy(item?.value)}
              inputId="select-sort"
            />
          </Box>
        </FormControl>
      </Grid>

      <Divider mb={4} />

      <Box pb={4}>
        {totalResults === 0 && !isLoading ? (
          <Alert variant="left-accent" status="error">
            {`No results found for ${query}. Try a different query.`}
          </Alert>
        ) : (
          <UniversityList universities={universities} />
        )}
      </Box>

      <Pagination
        currentPage={currentPage}
        totalResults={totalResults}
        onChangePage={setCurrentPage}
      />
    </Layout>
  );
};

/* istanbul ignore next */
export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req } = context;
  const token = req.cookies.token;
  let user = null;
  try {
    if (token) {
      user = await (await verifyAccessToken(token)).payload;
    }
  } catch (err) {}
  return {
    props: {
      user,
    },
  };
};

export default Home;
