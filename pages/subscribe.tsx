import { Box } from "@chakra-ui/layout";
import { Form, Formik } from "formik";
import React, { FC } from "react";
import { Layout } from "../components/layout";
import { object, string } from "yup";
import { InputControl, SubmitButton } from "formik-chakra-ui";
import {
  FormValueModel,
  useOnSubmitNewsletter,
} from "../hooks/useSubmitNewsletter";
import { verifyAccessToken } from "../lib/jwt";
import { GetServerSideProps } from "next";
import { User } from ".prisma/client";

const validationSchema = object().shape({
  email: string()
    .email("Please enter a valid email")
    .required("This field is required"),
});

type Props = {
  user?: User | null;
};

const Subscribe: FC<Props> = ({ user }) => {
  const onSubmit = useOnSubmitNewsletter();

  return (
    <Layout title="Subcribe to our newsletter" user={user}>
      <Formik<FormValueModel>
        initialValues={{
          email: "",
        }}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        <Form>
          <Box p={4} bg="gray.50" boxShadow="xs">
            <Box
              maxWidth={{
                lg: "80%",
              }}
              pb={2}
            >
              <InputControl
                inputProps={{ bg: "white" }}
                name="email"
                label="Enter your email"
                isRequired
              />
            </Box>
            <SubmitButton colorScheme="linkedin">Submit</SubmitButton>
          </Box>
        </Form>
      </Formik>
    </Layout>
  );
};

/* istanbul ignore next */
export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req } = context;
  const token = req.cookies.token;
  let user = null;
  try {
    if (token) {
      user = await (await verifyAccessToken(token)).payload;
    }
  } catch (err) {}
  return {
    props: {
      user,
    },
  };
};

export default Subscribe;
