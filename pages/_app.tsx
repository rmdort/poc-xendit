import type { AppProps } from "next/app";
import { ChakraProvider } from "@chakra-ui/react";
import React from "react";
import { SWRConfig } from "swr";
import { fetcher } from "../utils";

const swrConfig = {
  fetcher,
  revalidateOnFocus: false,
};

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider>
      <SWRConfig value={swrConfig}>
        <Component {...pageProps} />
      </SWRConfig>
    </ChakraProvider>
  );
}
export default MyApp;
