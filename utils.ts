export const fetcher = (input: RequestInfo) =>
  fetch(input).then((res) => res.json());

export const alphaSort = (a: string, b: string) => a.localeCompare(b);
