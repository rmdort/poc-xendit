import { Box, Link, Text } from "@chakra-ui/layout";
import { Heading } from "@chakra-ui/react";
import React, { FC } from "react";
import { University } from "../typings";

type Props = {
  universities?: University[];
};

export const UniversityList: FC<Props> = ({ universities }) => {
  return (
    <>
      {universities?.map((uni) => {
        const { web_pages, name, country } = uni;
        const link = web_pages[0];
        return (
          <Box
            key={name}
            mb={4}
            p={4}
            borderWidth={1}
            borderRadius="md"
            data-testid="university-item"
          >
            <Heading size="md">{name}</Heading>
            <Text>{country}</Text>
            <Link fontSize="sm" color="gray.500" fontStyle="italic" href={link}>
              {link}
            </Link>
          </Box>
        );
      })}
    </>
  );
};
