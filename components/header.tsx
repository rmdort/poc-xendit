import React, { FC } from "react";
import Link from "next/link";
import { Box, Flex, Button, Text, Link as ChakraLink } from "@chakra-ui/react";
import { User } from ".prisma/client";
import { useRouter } from "next/router";
import Cookies from "js-cookie";

type Props = {
  user?: User | null;
  hideNavigation?: boolean;
};

export const Header: FC<Props> = ({ user, hideNavigation }) => {
  const router = useRouter();
  const handleLogout = async () => {
    Cookies.remove("token");
    router.push("/");
  };
  return (
    <Box p={4} borderBottomWidth={1} borderBottomColor="gray.400" bg="gray.200">
      <Flex>
        <Link href="/" passHref>
          <Box display="flex" alignItems="center" cursor="pointer">
            <Box
              bg="darkturquoise"
              p={1}
              minW={10}
              fontWeight="bold"
              textAlign="center"
              fontSize="2xl"
              mr={2}
              textDecoration="underline"
            >
              U
            </Box>
            <Text fontWeight="bold">Uni Search</Text>
          </Box>
        </Link>
        <Box
          display="flex"
          flex={1}
          justifyContent="flex-end"
          alignItems="center"
          hidden={hideNavigation}
        >
          <Box display={["none", "none", "block"]}>
            <Link href="/" passHref>
              <Button variant="link" ml={4}>
                Home
              </Button>
            </Link>
            <Link href="/subscribe" passHref>
              <Button variant="link" ml={4}>
                Subscribe
              </Button>
            </Link>
          </Box>
          {user ? (
            <Box ml={4}>
              Welcome {user.name}{" "}
              <ChakraLink fontWeight="bold" onClick={handleLogout}>
                Logout
              </ChakraLink>
            </Box>
          ) : (
            <>
              <Link href="/login" passHref>
                <Button colorScheme="green" ml={4}>
                  Login
                </Button>
              </Link>
              <Link href="/register" passHref>
                <Button colorScheme="pink" ml={2}>
                  Register
                </Button>
              </Link>
            </>
          )}
        </Box>
      </Flex>
    </Box>
  );
};
