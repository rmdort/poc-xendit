import React, { FC, useMemo } from "react";
import Select from "react-select";
import useSWR from "swr";
import { Country } from "../typings";
import { fetcher } from "../utils";

type Props = {
  value?: string;
  onChange?: (value: string | undefined) => void;
  inputId?: string;
};

export const CountrySelect: FC<Props> = ({ value, onChange, inputId }) => {
  const { data: countries, error } = useSWR<Country[]>("/api/countries", {
    fetcher,
  });
  const isLoading = !countries && !error;
  const selectValue = useMemo(
    () => (value === void 0 ? void 0 : { value, label: value }),
    [value]
  );
  return (
    <Select
      isClearable
      options={countries}
      isLoading={isLoading}
      value={selectValue}
      onChange={(item) => onChange?.(item?.value)}
      placeholder=""
      inputId={inputId}
    />
  );
};
