import { Button } from "@chakra-ui/button";
import { ChevronLeftIcon, ChevronRightIcon } from "@chakra-ui/icons";
import { Box } from "@chakra-ui/layout";
import React, { FC } from "react";
import { PAGE_SIZE } from "../config";
import { usePagination } from "../hooks/usePagination";

type Props = {
  totalResults: number;
  currentPage: number;
  onChangePage?: (page: number) => void;
};
export const Pagination: FC<Props> = ({
  totalResults,
  currentPage,
  onChangePage,
}) => {
  const { pages, totalPages } = usePagination({
    totalResults,
    currentPage,
    pageSize: PAGE_SIZE,
  });
  return (
    <Box display="flex">
      <Button
        mr={1}
        isDisabled={currentPage === 1}
        onClick={() => onChangePage?.(Math.max(1, currentPage - 1))}
        data-testid="button-previous"
      >
        <ChevronLeftIcon />
        Previous
      </Button>
      <Box display={["none", "flex"]}>
        {pages.map((page, idx) => {
          if (typeof page === "string") {
            return (
              <Box as="span" pl={4} pr={4} key={`ellipsis_${idx}`}>
                ...
              </Box>
            );
          }
          return (
            <Button
              colorScheme={page === currentPage ? "green" : "gray"}
              onClick={() => onChangePage?.(Number(page))}
              ml={1}
              mr={1}
              key={page}
            >
              {page}
            </Button>
          );
        })}
      </Box>
      <Button
        ml={1}
        isDisabled={currentPage === totalPages || !totalPages}
        onClick={() => onChangePage?.(Math.min(totalPages, currentPage + 1))}
        data-testid="button-next"
      >
        Next
        <ChevronRightIcon />
      </Button>
    </Box>
  );
};
