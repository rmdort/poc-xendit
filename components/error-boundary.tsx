import React, { Component, ErrorInfo, ReactNode } from "react";

type fallbackProps = {
  resetError: () => void;
};

type Props = {
  children: ReactNode;
  onReset?: () => void;
  fallback: (props: fallbackProps) => React.ReactNode;
};

type State = {
  hasError: boolean;
};

class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false,
  };

  public static getDerivedStateFromError(_: Error): State {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  public resetError = () => {
    this.setState({ hasError: false });
    this.props.onReset?.();
  };

  public render() {
    if (this.state.hasError) {
      return this.props.fallback({ resetError: this.resetError });
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
