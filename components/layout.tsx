import { User } from ".prisma/client";
import { Box, Container, Divider, Heading } from "@chakra-ui/layout";
import Head from "next/head";
import type { FC } from "react";
import React from "react";
import { Header } from "./header";

type Props = {
  title: string;
  user?: User | null;
  hideNavigation?: boolean;
};

export const Layout: FC<Props> = ({
  title,
  user,
  children,
  hideNavigation,
}) => {
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <Header user={user} hideNavigation={hideNavigation} />
      <Container maxW="container.xl" pt={6} pb={6}>
        <Heading as="h1" pb={4}>
          {title}
        </Heading>
        <Divider mb={8} />
        <Box>{children}</Box>
      </Container>
    </>
  );
};
