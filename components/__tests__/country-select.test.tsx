import { render, screen, fireEvent } from "@testing-library/react";
import { CountrySelect } from "../country-select";
import fetchMock from "jest-fetch-mock";
import userEvent from "@testing-library/user-event";

const mockCountries = [
  { value: "singapore", label: "Singapore" },
  { value: "australia", label: "Australia" },
];

describe("<CountrySelect />", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  it("can render successfully", () => {
    const { baseElement } = render(<CountrySelect value={"singapore"} />);
    expect(baseElement).toBeTruthy();
  });

  it("will retrieve countries from the api", async () => {
    fetchMock.mockResponse(JSON.stringify(mockCountries));
    const handleChange = jest.fn();
    render(<CountrySelect onChange={handleChange} />);
    const input = await screen.findByRole("combobox");
    userEvent.type(input, "Singapore");
    userEvent.type(input, "{enter}");
    expect(handleChange).toBeCalledWith("singapore");
  });
});
