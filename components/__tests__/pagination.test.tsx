import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Pagination } from "./../pagination";

describe("<Pagination />", () => {
  it("can render pagination", async () => {
    render(<Pagination currentPage={1} totalResults={100} />);
    expect(await (await screen.findAllByRole("button")).length).toBe(6);
    expect(screen.getByTestId("button-previous")).toBeTruthy();
    expect(screen.getByTestId("button-next")).toBeTruthy();
  });

  it("will trigger page change", () => {
    const handleChange = jest.fn();
    const { rerender } = render(
      <Pagination
        currentPage={1}
        totalResults={100}
        onChangePage={handleChange}
      />
    );
    userEvent.click(screen.getByTestId("button-next"));
    expect(handleChange).toHaveBeenCalledWith(2);
    rerender(
      <Pagination
        currentPage={2}
        totalResults={100}
        onChangePage={handleChange}
      />
    );
    userEvent.click(screen.getByTestId("button-previous"));
    expect(handleChange).toHaveBeenCalledWith(1);
    userEvent.click(screen.getByText("2"));
    expect(handleChange).toHaveBeenCalledWith(2);
  });
});
