import { render, screen } from "@testing-library/react";
import { University } from "../../typings";
import { UniversityList } from "./../university-list";

const mockUniversities: University[] = [
  {
    name: "B",
    country: "Singapore",
    "state-province": "SG",
    alpha_two_code: "SG",
    domains: [],
    web_pages: ["http://www.google.com"],
  },
  {
    name: "A",
    country: "Japan",
    "state-province": "SG",
    alpha_two_code: "SG",
    domains: [],
    web_pages: [],
  },
];

describe("<UniversityList />", () => {
  it("can render universities", () => {
    render(<UniversityList universities={mockUniversities} />);
    expect(screen.getByText("Singapore")).toBeTruthy();
    expect(screen.getByText("A")).toBeTruthy();
    expect(screen.getByText("http://www.google.com")).toBeTruthy();
  });
});
