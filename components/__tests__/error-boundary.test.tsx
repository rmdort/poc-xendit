import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import ErrorBoundary from "../error-boundary";

const App = () => {
  throw new Error("Something went wrong");
};

describe("<ErrorBoundary />", () => {
  it("can render fallback successfully", () => {
    render(
      <ErrorBoundary fallback={() => <div>Fallback</div>}>
        <App />
      </ErrorBoundary>
    );
    expect(screen.findByTestId("Fallback")).toBeTruthy();
  });

  it("can render children if there is no error", () => {
    render(
      <ErrorBoundary fallback={() => <div>Fallback</div>}>
        <div>Hello world</div>
      </ErrorBoundary>
    );
    expect(screen.findByTestId("Hello world")).toBeTruthy();
  });

  it("will allow resetting errors", () => {
    const handleReset = jest.fn();
    render(
      <ErrorBoundary
        fallback={({ resetError }) => (
          <button data-testid="reset" onClick={resetError}>
            Fallback
          </button>
        )}
        onReset={handleReset}
      >
        <App />
      </ErrorBoundary>
    );
    userEvent.click(screen.getByRole("button"));
    expect(handleReset).toBeCalled();
  });
});
