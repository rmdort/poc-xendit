import { User } from ".prisma/client";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Header } from "../header";

const mockUser: User = {
  id: 1,
  name: "hello",
  email: "foo@foobar.com",
  password: null,
};

describe("<Header />", () => {
  it("can render succesfully", () => {
    render(<Header />);
    expect(screen.findByText("Uni")).toBeTruthy();
  });

  it("can logout user", () => {
    render(<Header user={mockUser} />);
    userEvent.click(screen.getByText("Logout"));
  });
});
