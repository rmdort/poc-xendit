import { render, screen, waitFor } from "@testing-library/react";
import { Layout } from "../layout";

describe("<Layout />", () => {
  it("can render title", () => {
    render(<Layout title="Hello world" />);
    waitFor(() => {
      expect(document.title).toEqual("Hello world");
    });
  });

  it("can render children", () => {
    render(
      <Layout title="Hello world">
        <div>Foobar</div>
      </Layout>
    );
    waitFor(() => {
      expect(screen.findByText("Foobar")).toBeTruthy();
    });
  });
});
