import { toast, useToast } from "@chakra-ui/react";
import { FormikConfig } from "formik";
import { useCallback } from "react";

export type FormValueModel = {
  email: string;
};

export const useOnSubmitNewsletter = () => {
  const toast = useToast();
  return useCallback<FormikConfig<FormValueModel>["onSubmit"]>(
    async (values, formikHelpers) => {
      formikHelpers.setSubmitting(true);
      try {
        await fetch("/api/subscribe", {
          method: "POST",
          body: JSON.stringify({
            email: values.email,
          }),
        })
          .then(async (response) => {
            if (!response.ok) {
              const text = await response.text();
              throw text;
            }
            return response;
          })
          .catch((err) => {
            throw err;
          });

        toast({
          title: "Thank you.",
          description: "Email has been added to the newsletter",
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      } catch (err) {
        formikHelpers.setErrors({
          email: String(err),
        });
      } finally {
        formikHelpers.setSubmitting(false);
      }
    },
    []
  );
};
