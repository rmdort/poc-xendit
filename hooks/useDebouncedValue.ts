import { useEffect, useRef, useState } from "react";

export const useDebouncedValue = (value: string, timeout: number = 300) => {
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedValue(value);
    }, timeout);

    return () => clearTimeout(timer);
  }, [value]);
  return debouncedValue;
};
