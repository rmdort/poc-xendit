import { useMemo } from "react";
type Props = {
  totalResults: number;
  currentPage: number;
  pageSize: number;
};

export const usePagination = ({
  totalResults,
  currentPage,
  pageSize,
}: Props) => {
  const totalPages = Math.ceil(totalResults / pageSize);
  const pages = useMemo(() => {
    const start = 1;
    const edges = 2;
    const ellipsis = "...";
    const left = Math.max(currentPage - edges, 0);
    const right = Math.min(currentPage + edges, totalPages);
    const list: (number | string)[] = [];
    for (let i = start; i <= totalPages; i++) {
      if (i === 1 || i === totalPages || totalPages < pageSize) {
        list.push(i);
      } else {
        if (i === right + 1 || i === left - 1) list.push(ellipsis);

        if (i <= right && i >= left) list.push(i);
      }
    }

    return list;
  }, [totalResults, currentPage, pageSize, totalPages]);
  return {
    pages,
    totalPages,
  };
};
