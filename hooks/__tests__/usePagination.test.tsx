import { renderHook } from "@testing-library/react-hooks";
import { usePagination } from "../usePagination";

describe("usePagination", () => {
  it("will output pages", () => {
    const { result } = renderHook(() =>
      usePagination({ totalResults: 100, pageSize: 5, currentPage: 1 })
    );
    expect(result.current.totalPages).toBe(20);
    expect(result.current.pages.length).toBe(5);
  });
});
