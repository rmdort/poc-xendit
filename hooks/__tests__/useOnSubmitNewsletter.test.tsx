import { renderHook } from "@testing-library/react-hooks";
import { useOnSubmitNewsletter } from "../useSubmitNewsletter";
import fetchMock from "jest-fetch-mock";
import { waitFor } from "@testing-library/dom";

const handleError = jest.fn();
const formikHelpersMock = {
  setSubmitting: () => undefined,
  setErrors: handleError,
};

describe("useOnSubmitNewsletter", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  it("can return onsubmit handler", () => {
    const { result } = renderHook(() => useOnSubmitNewsletter());
    expect(typeof result.current).toBe("function");
  });

  it("can return onsubmit handler", () => {
    fetchMock.mockResponse(JSON.stringify("success"));
    const { result } = renderHook(() => useOnSubmitNewsletter());
    result.current({ email: "abc@cdf.com" }, formikHelpersMock as any);
  });

  it("can return onsubmit handler", async () => {
    fetchMock.mockResponse(() => Promise.reject(new Error("bad email")));
    const { result } = renderHook(() => useOnSubmitNewsletter());
    await result.current({ email: "abc@cdf.com" }, formikHelpersMock as any);
    await waitFor(() => {
      expect(handleError).toHaveBeenCalledWith({ email: "Error: bad email" });
    });
  });

  it("can throw error if duplicate email is entered", async () => {
    const setErrorsHandler = jest.fn();
    const helpers = {
      ...formikHelpersMock,
      setErrors: setErrorsHandler,
    };
    const response = new Response("Duplicate email", {
      status: 409,
      statusText: "Something went wrong!",
    });

    fetchMock.mockResolvedValue(response);
    const { result } = renderHook(() => useOnSubmitNewsletter());
    await result.current({ email: "abc@cdf.com" }, helpers as any);
    await waitFor(() => {
      expect(setErrorsHandler).toHaveBeenCalledWith({
        email: "Duplicate email",
      });
    });
  });
});
