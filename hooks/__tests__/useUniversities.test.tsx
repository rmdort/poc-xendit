import { renderHook } from "@testing-library/react-hooks";
import { University } from "../../typings";
import { useUniversities } from "../useUniversities";
import fetchMock from "jest-fetch-mock";

const mockUniversities: University[] = [
  {
    name: "B",
    country: "Singapore",
    "state-province": "SG",
    alpha_two_code: "SG",
    domains: [],
    web_pages: [],
  },
  {
    name: "A",
    country: "Japan",
    "state-province": "SG",
    alpha_two_code: "SG",
    domains: [],
    web_pages: [],
  },
];
describe("hooks/useUniversities", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  it("retrieves a list of universities from the api", async () => {
    fetchMock.mockResponse(JSON.stringify(mockUniversities));
    const { result, waitForNextUpdate } = renderHook(() => useUniversities());
    expect(result.current.isLoading).toBe(true);
    await waitForNextUpdate();
    expect(result.current.universities?.length).toBe(2);
  });

  it("allow sorting of universities by name", async () => {
    fetchMock.mockResponse(JSON.stringify(mockUniversities));
    const { result, waitForNextUpdate } = renderHook(() =>
      useUniversities(void 0, void 0, "name")
    );
    expect(result.current.universities?.[0].name).toBe("A");
  });
});
