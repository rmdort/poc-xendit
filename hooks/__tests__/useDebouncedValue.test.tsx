import { act, renderHook } from "@testing-library/react-hooks";
import { useDebouncedValue } from "../useDebouncedValue";

describe("useDebouncedValue", () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  it("can return original value during first render", async () => {
    const { result, rerender } = renderHook((initialValue: string = "hello") =>
      useDebouncedValue(initialValue)
    );
    act(() => {
      jest.advanceTimersByTime(300);
    });

    expect(result.current).toBe("hello");
  });

  it("can return debounced value", async () => {
    const { result, rerender } = renderHook((initialValue: string = "hello") =>
      useDebouncedValue(initialValue)
    );
    rerender("foobar");

    act(() => {
      jest.advanceTimersByTime(300);
    });

    expect(result.current).toBe("foobar");
  });
});
