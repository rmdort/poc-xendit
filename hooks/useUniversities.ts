import { useMemo } from "react";
import useSWR from "swr";
import { BASE_URL } from "../config";
import { University } from "../typings";
import { fetcher } from "../utils";

type SortDirection = "asc" | "desc";

export const useUniversities = (
  query?: string,
  country?: string,
  sortBy?: keyof University,
  sortDirection?: SortDirection,
  from?: number,
  to?: number
) => {
  const { data: rawUniversities, error } = useSWR<University[]>(
    [query, country],
    (query, country = "") =>
      fetcher(`${BASE_URL}search?name=${query}&country=${country}`)
  );
  const universities = useMemo(() => {
    if (!sortBy) {
      return rawUniversities;
    }
    return rawUniversities?.slice(0).sort((a, b) => {
      return String(a[sortBy]).localeCompare(String(b[sortBy]));
    });
  }, [rawUniversities, sortBy]);
  const isLoading = !error && !universities;
  const totalResults = universities?.length ?? 0;
  return {
    universities: universities?.slice(from, to),
    totalResults,
    error,
    isLoading,
  };
};
