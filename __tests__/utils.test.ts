import { alphaSort } from "../utils";

describe("sorting", () => {
  it("can sort alphabetically", () => {
    expect(["2", "1"].sort(alphaSort)).toEqual(["1", "2"]);
  });
});
