import { SortOption } from "./typings";

export const BASE_URL = "http://universities.hipolabs.com/";
export const DEFAULT_SORT_OPTIONS: SortOption[] = [
  {
    value: "name",
    label: "Name",
  },
  {
    value: "country",
    label: "Country",
  },
];
export const PAGE_SIZE = 10;
