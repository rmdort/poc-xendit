import { Authentication } from "../auth";

describe("Authentication", () => {
  beforeEach(() => {
    process.env.JWT_KEY = "hello";
  });

  it("can login", async () => {
    const email = Math.random().toString();
    await Authentication.register({
      name: "name",
      email,
      password: "password",
    });
    const login = async () => await Authentication.login(email, "password");
    const response = await login();
    expect(response.accessToken).toBeTruthy();
  });

  it("will not allow multiple registrations", async () => {
    const email = Math.random().toString();
    const register = async () =>
      await Authentication.register({
        name: "name",
        email,
        password: "password",
      });
    await register();

    expect(register()).rejects.not.toThrow();
  });

  it("throws login error for incorrect user", async () => {
    const login = async () => await Authentication.login("name", "foobar");
    await expect(login()).rejects.toThrow("User not registered");
  });

  it("throws login error for incorrect password", async () => {
    const email = Math.random().toString();
    const register = async () =>
      await Authentication.register({
        name: "name",
        email,
        password: "password",
      });
    await register();
    const login = async () => await Authentication.login(email, "password2");
    await expect(login()).rejects.toThrow(
      "Email address or password not valid"
    );
  });

  it("can register user", async () => {
    const register = async () =>
      await Authentication.register({
        name: "name",
        email: Math.random().toString(),
        password: "foobar",
      });
    const response = await register();
    expect(response.accessToken).toBeTruthy();
  });
});
