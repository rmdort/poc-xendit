import { signAccessToken, verifyAccessToken } from "../jwt";

const mockUser: User = {
  id: 1,
  name: "hello",
  email: "foo@foobar.com",
  password: null,
};
describe("jwt", () => {
  beforeEach(() => {
    process.env.JWT_KEY = "hello";
  });
  it("can create a signed token", async () => {
    expect(await signAccessToken(mockUser)).toBeTruthy();
  });

  it("can throw error", async () => {
    delete process.env.JWT_KEY;
    const sign = async () => await signAccessToken(mockUser);
    expect(sign()).rejects.toThrow();
  });

  it("can verify token", async () => {
    expect(async () => await verifyAccessToken("string")).rejects.toThrow();
  });
});
