import jwt from "jsonwebtoken";
import { InternalServerError, Unauthorized } from "http-errors";
import { User } from "@prisma/client";

type UserTokenResponse = {
  payload: User;
};

export const signAccessToken = async (payload: User) => {
  return new Promise((resolve, reject) => {
    jwt.sign({ payload }, process.env.JWT_KEY as string, {}, (err, token) => {
      if (err) {
        reject(new InternalServerError());
      }
      resolve(token);
    });
  });
};

export const verifyAccessToken = async (
  token: string
): Promise<UserTokenResponse> => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_KEY as string, (err, payload) => {
      if (err) {
        const message =
          err.name == "JsonWebTokenError" ? "Unauthorized" : err.message;
        return reject(new Unauthorized(message));
      }
      resolve(payload as UserTokenResponse);
    });
  });
};
