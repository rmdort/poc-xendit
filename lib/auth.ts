import { User } from "@prisma/client";
import bcrypt from "bcryptjs";
import { signAccessToken } from "./jwt";
import { NotFound, Unauthorized } from "http-errors";
import { prisma } from "./client";

class Authentication {
  static async register(userData: Omit<User, "id">) {
    userData.password = bcrypt.hashSync(userData.password as string, 8);
    let user;
    try {
      user = await prisma.user.create({
        data: userData,
      });
      console.log("user", user);
    } catch (err) {
      throw "User already registered";
    }
    const accessToken = await signAccessToken(user);

    return {
      ...userData,
      password: void 0,
      accessToken,
    };
  }

  static async login(email: string, password: string) {
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (!user) {
      throw new NotFound("User not registered");
    }

    const checkPassword = bcrypt.compareSync(password, user.password as string);
    if (!checkPassword)
      throw new Unauthorized("Email address or password not valid");
    const accessToken = await signAccessToken(user);
    return { ...user, password: void 0, accessToken };
  }
}

export { Authentication };
