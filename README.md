## To test this POC

1. Clone this repository

   `git clone https://gitlab.com/rmdort/poc-xendit.git`

2. Install dependencies

   `npm i`

3. Run Nextjs dev server

   `npm run dev`

4. Open `http://localhost:3000` in Chrome

## Features

1. ~99% test coverage
2. Incremental pagination
3. Responsive design
4. Fully TypeScript
5. ESLint
6. University can be sorted by name, country
7. Hooks for separating application concerns
8. User registration + login
9. Subscription data is persisted in a JSON file under `public/subscriptions.json`

Note:

- Did not implement user favorites
- User information is stored using Prisma in sqlite

## Linting

`npm run lint` will run linting with nextjs lint rules
